/*
Write a program that calls fork(). Before calling fork(), have the main process access a variable (e.g., x)
and set its value to some thing (e.g., 100). What value is the variable in the child process?
What happens to the variable when both the child and parent change the value of x?
*/

use nix::unistd::{fork, ForkResult};
use nix::sys::wait;

fn main() {
    let x: i32 = 100;
    println!("Before fork() is called, x: {}", x);

    match fork() {
        Ok(ForkResult::Parent { child: _ }) => {
            match wait::wait() {
                Ok(_status) => (),
                Err(_err) => (),
            }
            println!("The value of x as the parent inherets it: {}", x);
            
            let x: i32 = 120;
            println!("The value of x after the parent changes it: {}", x);
            
        }
        Ok(ForkResult::Child) => {
            println!("The value of x as the child inherits is: {}", x);

            let x: i32 = 80;
            println!("the value of x after the child modifies it: {}", x);
        }
        Err(_) => println!("Fork failed"),
    }
}

