/*
Write a program that opens a file (with the open() system call) and then calls fork() to create a new process.
Can both the child and parent access the file descriptor returned by open()?
What happens when they are writing to the file concurrently?
*/

use nix::unistd::{fork,ForkResult,sleep};
use std::fs::OpenOptions;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let mut options = OpenOptions::new();
    let mut f = options.read(true).write(true).open("/home/centos/operatingSystems/1_virtualization/processAPI/homework/rust/_2/src/file_2")?;

    //f.write(b"This should be in the file_2 file\n")?;

    match fork() {
        Ok(ForkResult::Parent {child: _}) => {
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            sleep(1);
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
            f.write(b"Written from the parent process\n")?;
    
        }
        Ok(ForkResult::Child) => {
            f.write(b"Written from the child process\n")?;
            f.write(b"Written from the child process\n")?;
            f.write(b"Written from the child process\n")?;
            f.write(b"Written from the child process\n")?;
            f.write(b"Written from the child process\n")?;
            f.write(b"Written from the child process\n")?;
            f.write(b"Written from the child process\n")?;
            f.write(b"Written from the child process\n")?;
            f.write(b"Written from the child process\n")?;
            f.write(b"Written from the child process\n")?;
        }
        Err(err) => println!("{}",err),
    }


    Ok(())
}

