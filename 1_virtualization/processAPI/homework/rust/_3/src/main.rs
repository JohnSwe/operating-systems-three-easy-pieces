/*
Write another program using fork(). The child process should print “hello”; the parent process should print “goodbye”. 
You should try to ensure that the child process always prints first; 
can you do this without calling wait() in the parent?
*/
use nix::unistd::{fork, getpid, getppid, ForkResult};
use nix::sys::signal::{self, Signal};
use nix::sys::wait::waitpid;

fn main() {
    match fork() {
        Ok(ForkResult::Parent{child}) => {
            waitpid(child, None).unwrap();
            println!("goodbye from the parent process! My pid is {}", getpid());
        }
        Ok(ForkResult::Child) => {
            println!("hello from the child process! My pid is {}", getpid());
        }
        Err(err) => println!("{}", err),
    }
}
