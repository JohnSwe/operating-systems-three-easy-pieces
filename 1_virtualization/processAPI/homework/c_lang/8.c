/*
Write a program that creates two children, and connects the standard output of one to the standard input of the other, using the pipe() system call.
*/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char* argv[]){
    int shell_pid = (int) getppid();
    kill(shell_pid, SIGSTOP);

    char buf;
    int pipefd[2];
    if (pipe(pipefd) < 0){
        fprintf(stderr, "pipe failed");
        exit(1);
    }

    int rc_1 = fork();

    if (rc_1 < 0){
        fprintf(stderr, "fork1 failed");
        exit(1);
    } else if (rc_1 == 0) {
        //child1, writing to the pipe
        
        close(pipefd[0]); //close the read end

        write(pipefd[1], argv[1], strlen(argv[1]));
        close(pipefd[1]); //close the write end


    } else {
        int rc_2 = fork();
        if (rc_2 < 0){
            fprintf(stderr, "fork2 failed");
            exit(1);
        } else if (rc_2 == 0) {
            //child2, reading from the pipe

            close(pipefd[1]); //close the write end

            while (read(pipefd[0], &buf, 1) > 0) {
                printf("Before\n");
                write(STDOUT_FILENO, &buf, 1);
                write(STDOUT_FILENO, "\n", 1);
                printf("After\n");
            }

            write(STDOUT_FILENO, "\n", 1);

            close(pipefd[0]);  //close the read end
            
            kill(shell_pid, SIGCONT);
        }
    }

    return 0;
}
