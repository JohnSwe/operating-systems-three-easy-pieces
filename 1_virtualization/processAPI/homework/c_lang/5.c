/*
Now write a program that uses wait() to wait for the child processto finish in the parent. 
What does wait() return? What happens if you usewait()in the child?
*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){
    int rc = fork();

    if (rc < 0){
        //fork failed
        fprintf(stderr, "Fork failed");
    } else if (rc == 0) {
        //child
        printf("Printing from the child process (%d)\n", (int) getpid());
    } else {
        //parent
        int wait_rc = wait(NULL);
        printf("Printing from the parent process (%d)\n", (int) getpid());
        printf("The wait() call returns: %d\n", wait_rc);
    } 
}

//// using wait() in the child process ////
//// The return code is -1 ////
/*
int main(int argc, char *argv[]){
    int rc = fork();

    if (rc < 0){
        //fork failed
        fprintf(stderr, "Fork failed");
    } else if (rc == 0) {
        //child
        int wait_rc = wait(NULL);
        printf("Printing from the child process (%d)\n", (int) getpid());
        printf("The wait() call returns: %d\n", wait_rc);

    } else {
        //parent
        printf("Printing from the parent process (%d)\n", (int) getpid());
    } 
}
*/
