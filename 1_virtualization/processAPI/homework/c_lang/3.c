#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    int shell_pid = (int) getppid();
    kill(shell_pid, SIGSTOP);      //Pause the shell process while the program runs

    int rc = fork();

    if (rc < 0) {
        //fork failed
        exit(1);
    } else if (rc == 0) {
        //child
        printf("Hello from the child process (%d)\n", (int) getpid());
        //kill((int)getpid()-1, SIGCONT); //Resume the stopped parent process
    } else {
        //parent
        kill((int)getpid(), SIGSTOP); //Stops the parent process
        printf("Goodbye from the parent process (%d)\n", (int) getpid());
        //maybe resume shell here

        kill(shell_pid, SIGCONT);
    }

    kill(0, SIGCONT);

    return 0;
}
