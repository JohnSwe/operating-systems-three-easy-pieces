/*
Write a program that creates a child process, and then in the child closes standard output (STDOUTFILENO). 
What happens if the childcallsprintf()to print some output after closing the descriptor?
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]){
    int rc = fork();

    if (rc < 0){
        fprintf(stderr, "fork failed");
        exit(1);
    } else if (rc == 0) {
        //child process
        close(STDOUT_FILENO);
        printf("Will this print?????");
    } else {
        printf("Parent process checking in, is everything ok down there?\n");
    }

    return 0;
}
