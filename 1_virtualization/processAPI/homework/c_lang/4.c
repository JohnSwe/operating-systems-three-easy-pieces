#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char *argv[]){
    int rc = fork();

    if (rc < 0) {
        fprintf(stderr, "fork failed");
        exit(1);
    } else if (rc == 0) {
        //child process
        
        char *command[3];
        command[0] = strdup("/bin/ls");
        command[1] = strdup("-la");
        command[2] = NULL;

        char *env[2];
        env[0] = strdup("HOME=/home/jimmyjam");
        env[1] = NULL;

        printf("------execvpe-----\n");
        fflush(stdout);
        execvpe(command[0], command, env);

        printf("------execvp------\n");
        fflush(stdout);
        execvp(command[0], command);

        printf("------execv-------\n");
        fflush(stdout);
        execv(command[0], command);

        printf("------execlp------\n");
        fflush(stdout);
        execlp("ls", "ls", command[1], command[2]);

        printf("------execle-----\n");
        fflush(stdout);
        execle(command[0], command[0], command[1], command[2], env);
        
        printf("------execl-------\n");
        fflush(stdout);
        execl(command[0], command[0], command[1], command[2]);

        
    } else {
        //parent
        wait(NULL);
        printf("Printing from parent\n");
    }

    return 0;
}
