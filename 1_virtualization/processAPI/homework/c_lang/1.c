/*
Write a program that calls fork(). Before calling fork(), have themain process access a variable (e.g., x) 
and set its value to some-thing (e.g., 100). What value is the variable in the child process?
What happens to the variable when both the child and parent changethe value of x?
*/

#include <stdio.h>
#include <stdlib.h>

int main(int argx, char *argv[]) {
    int x = 100;
    printf("Before fork() is called, x: %d\n\n", x);

    int rc = fork();
    if (rc < 0) {
        //fork failed
        fprintf(stderr, "Forked failed, exiting the program...");
        exit(1);
    } else if (rc == 0) {
        //child process
        printf("The value of x as the child inherits it: %d\n", x);

        x = 80;
        printf("The value of x after the child changes it: %d\n", x);
        printf("\n");
    } else {
        //parent's execution path
        wait(NULL);

        printf("The value of x as the parent inherits it: %d\n", x);

        x = 120;
        printf("the value of x after the parent changes it: %d\n", x);
    }

    printf("\n");
    return 0;
}
