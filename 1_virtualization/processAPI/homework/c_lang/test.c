#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>

int main(int argc, char *argv[]) {
    int rc = fork();


    char *arr[5];
    arr[0] = strdup("The");
    arr[1] = strdup("quick");
    arr[2] = strdup("brown");
    arr[3] = strdup("fox");
    arr[4] = strdup("jumped");

    printf(*arr);

    if (rc < 0){
        //fork failed
        fprintf(stderr, "fork failed\n");
    } else if (rc == 0) {
        int parent_proc = (int) getpid()-1;
        kill(parent_proc, SIGSTOP);
    } else {
        int child_proc = (int) getpid()+1;
        kill(child_proc, SIGSTOP);
    }
}
