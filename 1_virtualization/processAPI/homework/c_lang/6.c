/*
Write a slight modification of the previous program, this time using wait pid() instead of wait(). When would waitpid() be useful?
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    int shell_pid = (int) getppid();
    kill(shell_pid, SIGSTOP);      //Pause the shell process while the program runs

    int rc = fork();

    if (rc < 0) {
        fprintf(stderr,"fork failed");
        exit(1);
    } else if (rc == 0) {
        //child
        printf("Printing from the child process (%d)\n", (int)getpid());
    } else {
        //parent

        printf("rc: %d\n", rc);
        printf("getpid()+1: %d\n", (int) getpid()+1);        

        int pid = rc;
        int status;
        
        waitpid(rc, &status, 0);
        printf("Printing from the parent process (%d)\n", (int)getpid());

        kill(shell_pid, SIGCONT);
    }

    return 0;
}


/*
Interesting lesson learend in this one. I made the mistake of using rc+1 as the first argument in waitpid().
fork(), for the parent process, returns the pid of the child process created, so rc+1 in this case is the pid of the parent process.
This caused the ~shell~ to wait for the 'parent' process in this program (the initally run program).
So the shell process returned and the prompt is displayed before the parent's line is printed.

Try switching in 'rc+1' as the first argument to waitpid() on line 31 to see this effect.
*/
